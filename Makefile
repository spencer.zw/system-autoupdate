#
PREFIX = /
SYSTEMD_UNIT_DIR = /lib/systemd
DATADIR = /usr/share

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	install -d $(DESTDIR)/$(PREFIX)/usr/sbin
	install -d $(DESTDIR)/$(PREFIX)/usr/bin
	install -m0755 ./usr/sbin/system-autoupdate $(DESTDIR)/$(PREFIX)/usr/sbin/system-autoupdate
	install -m0755 ./usr/sbin/system-autoupdate-runner $(DESTDIR)/$(PREFIX)/usr/sbin/system-autoupdate-runner
	cd $(DESTDIR)/$(PREFIX)/usr/bin; ln -s ../sbin/system-autoupdate-runner saur
	
	install -d $(DESTDIR)/$(PREFIX)/usr/share/polkit-1/rules.d
	install -m0644 ./usr/share/polkit-1/rules.d/90-system-autoupdate.rules $(DESTDIR)/$(PREFIX)/usr/share/polkit-1/rules.d/90-system-autoupdate.rules
	
	install -d $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/restart-services
	install -d $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/update-scripts
	install -m0644 ./usr/share/system-autoupdate/common-funcs.sh $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/common-funcs.sh
	install -m0644 ./usr/share/system-autoupdate/restart-services/debian-ubuntu.list $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/restart-services/debian-ubuntu.list
	install -m0644 ./usr/share/system-autoupdate/update-scripts/alt.sh $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/update-scripts/alt.sh
	install -m0644 ./usr/share/system-autoupdate/update-scripts/arch.sh $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/update-scripts/arch.sh
	install -m0644 ./usr/share/system-autoupdate/update-scripts/centos.sh $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/update-scripts/centos.sh
	install -m0644 ./usr/share/system-autoupdate/update-scripts/debian-ubuntu.sh $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/update-scripts/debian-ubuntu.sh
	install -m0644 ./usr/share/system-autoupdate/update-scripts/fedora.sh $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/update-scripts/fedora.sh
	install -m0644 ./usr/share/system-autoupdate/update-scripts/rosa.sh $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/update-scripts/rosa.sh
	install -m0644 ./usr/share/system-autoupdate/update-scripts/suse.sh $(DESTDIR)/$(PREFIX)/usr/share/system-autoupdate/update-scripts/suse.sh
	
	install -d $(DESTDIR)/$(PREFIX)/$(SYSTEMD_UNIT_DIR)/system
	install -d $(DESTDIR)/$(PREFIX)/$(SYSTEMD_UNIT_DIR)/system-preset
	install -m0644 ./lib/systemd/system/system-autoupdate-reboot-guard.service $(DESTDIR)/$(PREFIX)/$(SYSTEMD_UNIT_DIR)/system/system-autoupdate-reboot-guard.service
	install -m0644 ./lib/systemd/system/system-autoupdate-sanity.service $(DESTDIR)/$(PREFIX)/$(SYSTEMD_UNIT_DIR)/system/system-autoupdate-sanity.service
	install -m0644 ./lib/systemd/system/system-autoupdate.service $(DESTDIR)/$(PREFIX)/$(SYSTEMD_UNIT_DIR)/system/system-autoupdate.service
	install -m0644 ./lib/systemd/system/system-autoupdate-start-reboot-guard.service $(DESTDIR)/$(PREFIX)/$(SYSTEMD_UNIT_DIR)/system/system-autoupdate-start-reboot-guard.service
	install -m0644 ./lib/systemd/system/system-autoupdate.timer $(DESTDIR)/$(PREFIX)/$(SYSTEMD_UNIT_DIR)/system/system-autoupdate.timer
	install -m0644 ./lib/systemd/system-preset/85-system-autoupdate.preset $(DESTDIR)/$(PREFIX)/$(SYSTEMD_UNIT_DIR)/system-preset/85-system-autoupdate.preset
	
	install -d $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/restart-services
	install -m0644 ./etc/system-autoupdate/restart-services/example.list $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/restart-services/example.list
	install -m0644 ./etc/system-autoupdate/restart-services/general.list $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/restart-services/general.list
	install -m0644 ./etc/system-autoupdate/system-autoupdate.conf $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/system-autoupdate.conf
	
	install -d $(DESTDIR)/$(PREFIX)/etc/sudoers.d
	install -m 0440 ./etc/sudoers.d/system-autoupdate-polkit $(DESTDIR)/$(PREFIX)/etc/sudoers.d/system-autoupdate-polkit
	
	install -d $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/hooks/pre-update
	install -d $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/hooks/post-update
	install -d $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/hooks/on-shutdown-error
	install -m0644 ./etc/system-autoupdate/hooks/on-shutdown-error/10-on-shutdown-error.sh $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/hooks/on-shutdown-error/10-on-shutdown-error.sh
	install -m0644 ./etc/system-autoupdate/hooks/post-update/10-post-update.sh $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/hooks/post-update/10-post-update.sh
	install -m0644 ./etc/system-autoupdate/hooks/pre-update/10-pre-update.sh $(DESTDIR)/$(PREFIX)/etc/system-autoupdate/hooks/pre-update/10-pre-update.sh
	
uninstall:
	rm -fv $(PREFIX)/usr/sbin/system-autoupdate
	rm -fv $(PREFIX)/usr/sbin/system-autoupdate-runner
	rm -fvr $(PREFIX)/usr/share/system-autoupdate/
	rm -fv $(PREFIX)/$(SYSTEMD_UNIT_DIR)/system/system-autoupdate*
	rm -fvr $(PREFIX)/etc/system-autoupdate/
	rm -fv $(PREFIX)/etc/sudoers.d/system-autoupdate-polkit
