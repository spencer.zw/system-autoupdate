Name: system-autoupdate
Summary: Automatically update system and block shutdown
Version: 1.15
Release: alt1
License: GPL
Group: System/Configuration/Packaging
Url: https://gitlab.com/mikhailnov/system-autoupdate
BuildArch: noarch
Source0: %name-%version.tar

BuildRequires: pkgconfig(systemd)

#AutoReq: no
Requires: systemd, notify-send, sudo, update-kernel
# Requiring 'polkit' is not really necessary because it may be intentionally not installed where it's not needed,
# system-autoupdate will work without polkit,
# but than the directory /usr/share/polkit-1/rules.d does not belong to any package, so we depend from polkit
Requires: polkit

%description
Auto updates system using systemd capabilities and prevents it from being shut down while updating.
Shows notifications to user when attempting to shutdown during update.
Allows blocking shutdown only without auto aupdates.
.
Run 'saur enable' after installation to enable autoupdate.

%description -l ru_RU.UTF-8
Автоматическое обновление системы с использованием возможностей systemd
и блокировкой выключения системы во время обновления.
Можно использовать только для блокировки выключения без автообновлений.
.
После установки выполните 'saur enable', чтобы включить автообновления.

%prep
%setup

%install
%makeinstall_std
# remove scripts for updating other disctributions but ALT
# It's not really needed when 'AutoReq: no'
find %{buildroot}/usr/share/system-autoupdate/update-scripts/ ! -name 'alt.sh' -type f -exec rm -fv {} +

%post
#!/bin/sh
# http://meinit.nl/rpm-spec-prepostpreunpostun-argument-values
# This is an initial install
if [ "$1" = '1' ]; then
if echo "$LANG" | grep -Ei "^ru_|^ru$" | grep -Eiq 'utf8|utf-8'
	then echo "Выполните 'saur enable' для включения автообновлений, 'saur disable' для их выключения."
	else echo "Run 'saur enable' to turn on autoupdates, 'saur disable' to turn them off."
fi
fi

%preun
#!/bin/sh
# This is a full uninstall
# Unblock shutdown on uninstallation
[ "$1" = '0' ] && %{_sbindir}/system-autoupdate-runner disable || :

%files
%_datadir/polkit-1/rules.d/*
%dir %_datadir/system-autoupdate
%_datadir/system-autoupdate/*
%_presetdir/*
%_unitdir/*
%_sbindir/*
%_bindir/*

# sudoers configs are an important part of this packages
# sudoers configs must not be 'noreplace'
%config %_sysconfdir/sudoers.d/*
%dir %_sysconfdir/system-autoupdate
%config %_sysconfdir/system-autoupdate/*

%changelog
* Fri Feb 01 2019 Mikhail Novosyolov <mikhailnov@altlinux.org> 1.15-alt1
- Version 1.15
* Mon Nov 12 2018 Mikhail Novosyolov <mikhailnov@altlinux.org> 1.14-alt1
- Version 1.14
- First upload to Sisyphus
* Tue Nov 06 2018 Mikhail Novosyolov <mikhailnov@altlinux.org> 1.13-alt1
- Version 1.13
- Spec rework
* Thu Aug 30 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 1.12-alt1
- Version 1.12:
- Improved systemd presets
- Fixed sending notifications on ALT Linux
* Tue Jul 31 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 1.11-alt1
- Version 1.11
- Important fix: changed 'apt-get -f install' to 'apt-get -f -y install'
* Sat Jul 28 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 1.10-alt2
- Cleanup spec
- Disable AutoReq
- Postinstall bad hack to call 'control', otherwise /usr/bin/sudo
  has permissions 4710 and cannot be called from polkit rule
* Sat Jul 28 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 1.10-alt1
- New version 1.10
* Fri Jul 27 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 1.9-alt1
- initial RPM build

